-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema HomeWork1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema HomeWork1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `HomeWork1` DEFAULT CHARACTER SET utf8 ;
USE `HomeWork1` ;

-- -----------------------------------------------------
-- Table `HomeWork1`.`PRODUCT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `HomeWork1`.`PRODUCT` (
  `maker` VARCHAR(10) NOT NULL,
  `model` VARCHAR(50) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`model`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `HomeWork1`.`LAPTOP`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `HomeWork1`.`LAPTOP` (
  `code` INT NOT NULL,
  `model` VARCHAR(50) NOT NULL,
  `speed` INT NOT NULL,
  `ram` INT NOT NULL,
  `hd` REAL NOT NULL,
  `price` DECIMAL NULL,
  `screen` INT NOT NULL,
  `PRODUCT_model` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`code`, `PRODUCT_model`),
  INDEX `fk_LAPTOP_PRODUCT_idx` (`PRODUCT_model` ASC) VISIBLE,
  CONSTRAINT `fk_LAPTOP_PRODUCT`
    FOREIGN KEY (`PRODUCT_model`)
    REFERENCES `HomeWork1`.`PRODUCT` (`model`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `HomeWork1`.`PRINTER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `HomeWork1`.`PRINTER` (
  `code` INT NOT NULL,
  `model` VARCHAR(45) NOT NULL,
  `color` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `price` DECIMAL NULL,
  `PRODUCT_model1` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`code`, `PRODUCT_model1`),
  INDEX `fk_PRINTER_PRODUCT1_idx` (`PRODUCT_model1` ASC) VISIBLE,
  CONSTRAINT `fk_PRINTER_PRODUCT1`
    FOREIGN KEY (`PRODUCT_model1`)
    REFERENCES `HomeWork1`.`PRODUCT` (`model`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `HomeWork1`.`PC`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `HomeWork1`.`PC` (
  `code` INT NOT NULL,
  `model` VARCHAR(45) NULL,
  `speed` INT NULL,
  `ram` INT NULL,
  `hd` REAL NULL,
  `cd` VARCHAR(45) NULL,
  `price` DECIMAL NULL,
  `PRODUCT_model` VARCHAR(50) NULL,
  PRIMARY KEY (`code`),
  INDEX `fk_PC_PRODUCT1_idx` (`PRODUCT_model` ASC) VISIBLE,
  CONSTRAINT `fk_PC_PRODUCT1`
    FOREIGN KEY (`PRODUCT_model`)
    REFERENCES `HomeWork1`.`PRODUCT` (`model`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
